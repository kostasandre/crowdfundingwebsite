﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using VIva2DataAccess;
using WebApiViva2.Controllers;

namespace WebApiViva2.Controllers
{
    public class CreateProjectController: ApiController
    {
        public HttpResponseMessage Post([FromBody]Projects project)//[FromBody]Projects project
        {
            HttpResponseMessage response = new HttpResponseMessage();
            if (UsersSecurity.user_id != 0)
            {
                using (DreamFountainDBEntities entities = new DreamFountainDBEntities())
                {
                    project.User_ID = UsersSecurity.user_id;
                    var entity = entities.Projects.Add(project);
                    entities.SaveChanges();
                }
                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest);
                
            }
            return response;

        }
    }
}