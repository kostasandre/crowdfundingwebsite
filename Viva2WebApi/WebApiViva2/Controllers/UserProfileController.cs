﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using VIva2DataAccess;

namespace WebApiViva2.Controllers
{
    
    public class UserProfileController : ApiController
    {
        public string Get(string password)
        {
            if(UsersSecurity.user_id != 0)
            {
                using (DreamFountainDBEntities entities = new DreamFountainDBEntities())
                {
                    var user = entities.Users.FirstOrDefault(x => x.User_ID == UsersSecurity.user_id);
                    var userPassword = user.Password;
                    return userPassword;
                }
            }
            else
            {
                return null;
            }
            
        }

        // GET api/values
        //public IEnumerable<string> Get()
        //{

        //    return new string[] { "value1", "value2" };
        //}


        [BasicAuthenticationAttributes]
        // GET api/values/5
        public Users GetUserInfo()
        {
            using (DreamFountainDBEntities entities = new DreamFountainDBEntities())
            {
                return entities.Users.FirstOrDefault(e => e.User_ID == UsersSecurity.user_id);
            }
        }

        // POST api/values
        //[BasicAuthenticationAttributes]
        public void Post([FromBody]Users user)
        {
            if (UsersSecurity.user_id != 0)
            {
                using (DreamFountainDBEntities entities = new DreamFountainDBEntities())
                {
                    var entity = entities.Users.FirstOrDefault(x => x.User_ID == UsersSecurity.user_id);
                    entity.FirstName = user.FirstName;
                    entity.LastName = user.LastName;
                    entity.Email = user.Email;
                    entities.SaveChanges();
                }
            }
        }

        public void Post(int id, [FromBody]Users user)
        {
            if (UsersSecurity.user_id != 0)
            {
                using (DreamFountainDBEntities entities = new DreamFountainDBEntities())
                {
                    var entity = entities.Users.FirstOrDefault(x => x.User_ID == UsersSecurity.user_id);
                    entity.Password = user.Password;
                    entities.SaveChanges();
                }
            }
        }



        // PUT api/userprofile/5
        //[BasicAuthenticationAttributes]
        public void Put(string password)
        {


            if (UsersSecurity.user_id != 0)
            {
                using (DreamFountainDBEntities entities = new DreamFountainDBEntities())
                {
                    var user = entities.Users.FirstOrDefault(x => x.User_ID == UsersSecurity.user_id);
                    user.Password = password;
                    entities.SaveChanges();
                }
            }
        }


        //[BasicAuthenticationAttributes]
        //public void Put(string password)
        //{
        //    using (DreamFountainDBEntities entities = new DreamFountainDBEntities())
        //    {
        //        var entity = entities.Users.FirstOrDefault(x => x.User_ID == 1);
        //        entity.Password = password;
        //        entities.SaveChanges();
        //    }
        //}

        // DELETE api/values/5
        public void Delete(int id)
        {
        }        
    }
}
